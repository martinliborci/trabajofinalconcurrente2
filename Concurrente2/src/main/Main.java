package main;

import java.util.ArrayList;

import monitor.*;
import  petri.*;
import semaforo.*;

public class Main {
	//////////monitor
	static Monitor m= new Monitor();
	//////////vectores de tranciones
	static ArrayList<Integer> produce=new ArrayList<Integer>();
	static ArrayList<Integer> consume=new ArrayList<Integer>();

	public static void main(String[] args) {
		
		produce.add(0);
		produce.add(1);
		
		consume.add(3);
		consume.add(2);
		

		
		new ThreadConRunnable("productor",produce,m);
		new ThreadConRunnable("consumidor",consume,m);
	
	}
}

