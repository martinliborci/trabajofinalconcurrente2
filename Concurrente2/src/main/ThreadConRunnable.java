package main;

import java.util.ArrayList;
import monitor.*;
import  petri.*;
import semaforo.*;

public class ThreadConRunnable implements Runnable {
	private String nombreHilo;
	private ArrayList<Integer> transiciones;//conjunto de transicones que se deben disparar
	private Monitor m;
	private int actualPosicion;
	public ThreadConRunnable(String _nombreHilo,ArrayList<Integer> _transiciones, Monitor m) {

		this.nombreHilo = _nombreHilo;
		Thread t=new Thread(this,_nombreHilo);
		this.m=m;
		transiciones=new ArrayList<Integer>(_transiciones);
		 this.actualPosicion=0;
		t.start(); // Start the thread
	}

	public void run() {
	//	while (true) {}
		//System.out.println(nombreHilo);
		//m.entrar();
		//imprimeTransiciones();
		while(true) {
		
			if(actualPosicion==transiciones.size()) {
				actualPosicion=0; // lo vuelvo a recorrer al vector de transciones
			}
			
				try {
					Thread.currentThread();
					Thread.sleep(500);
					//agrego un system pause para testear
					System.out.println();
					new java.util.Scanner(System.in).nextLine();
					m.entrar(transiciones.get(actualPosicion));// entra el hilo con la transicion a disparar	
					actualPosicion++;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
	}
	
	public void imprimeTransiciones() {

	}
	
	
	/////////privates
	
	
	}
	
