package main;

import java.util.ArrayList;
import java.util.Random;

public class Politica {

	public Politica() {	}
	/**
	 * 
	 * @param _m	vector que me indica transiciones sensibilizadas y con hilos dormidos
	 * @return		entero de la transición a despertar
	 */
	public int decidir(int[]_m) {
		System.out.println("vector m");
		for(int i=0;i< _m.length;i++) {
			System.out.println(_m[i]+"");
			
		}
		//genero un arraylist con las transiciones posibles a disparar
		ArrayList<Integer> transiciones = new ArrayList<Integer>();
		for(int i=0;i<_m.length;i++) {
			if(_m[i]!=0){
				transiciones.add(i);
			}
		}
		Random generadorAleatorios = new Random();
	    //genera un numero entre 1 y 5 y lo guarda en la variable numeroAleatorio
	    int numeroAleatorio = generadorAleatorios.nextInt(transiciones.size());
	    System.out.println("numeroAleatorio"+numeroAleatorio);
		return transiciones.get(numeroAleatorio);
	}
	
// metodo politica con la matriz prioridad
	public int decidir(int[]and, int[][] matriz_Prioridad){
		System.out.println("entramos a politica");
		int longitud= and.length;
		int[] aux=new int[longitud];
		int transicion=0;
		
		//construyo el vector de transiciones ordenadas por prioridad
		//osea: multiplico and por la matriz priodidad transpuesta
		for (int f=0;f<longitud;f++){
			aux[f]=0;
			for(int c=0;c<longitud;c++){
				aux[f]=aux[f]+(matriz_Prioridad[f][c]*and[c]);
			}
		}

		
		//recorro el resultado anterior (aux) hasta encontrar el primer 1 (transicion 
		//de mayor prioridad) y el resto lo pongo en 0
		
		boolean bandera=true;
		for (int j=0;j<longitud;j++){
			if (aux[j]==1 && bandera){
				bandera=false;
			}else{
				aux[j]=0;
			}
		}
		//multiplico el vector con un solo 1 por la matriz prioridad sin transponer 
		//para obtener la transici�n a disparar
		int[] aux2=new int[longitud];
		for (int c=0;c<longitud;c++){
			aux2[c]=0;
			for(int f=0;f<longitud;f++){
				aux2[c]=aux2[c]+(matriz_Prioridad[f][c]*aux[f]);
			}
		}
		
//		//-----------------------------------------------------
//		System.out.println("Vector aux2");		
//		for (int i=0;i<longitud;i++){
//					System.out.print(aux2[i]);
//				}
//				System.out.println();
//			//	new java.util.Scanner(System.in).nextLine();
//				
//		//-----------------------------------------------------
		//recorro vector aux2 y donde tengo un 1 guardo la posicion que es la transicion a disparar 
		for (int k=0;k<longitud;k++){
			if (aux2[k]==1) {
				transicion=k;
			}
//		}
		
	}
		return transicion;
}
}
