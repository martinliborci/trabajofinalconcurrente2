package semaforo;

/**
 * @author Domingo
 * 
 */
public class SemaforoBinario {

	protected int contador = 0;

	public SemaforoBinario(int valorInicial) { /*"valorinicial" siempre es 1 en este caso*/
		contador = valorInicial;
	}

	synchronized public void WAIT() {
		while (contador == 0) {
			try {
				wait();
			} catch (Exception e) {
			}
		}
		contador--;
	}

	synchronized public void SIGNAL() {
		contador = 1;
		notify(); /* despertamos a uno porque todos esperan por la misma condici�n */
	}
	
	//ver por qué está esto
	public int contador() {
		return contador;
	}
}
