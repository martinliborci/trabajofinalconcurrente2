package semaforo;

import java.util.Enumeration;
import java.util.Vector;



public class SemaforoBinarioFIFO extends SemaforoBinario {

	public Vector<Thread> bloqueados; /*vector de hilos bloqueados*/
	boolean condSalida;

	public SemaforoBinarioFIFO(int valorInicial, int tamanioColaBloqueados) {
		super(valorInicial);
		//no se si va "Vector" a secas o "Vector<Thread>" VERRRRR
		bloqueados  = new Vector<Thread>(tamanioColaBloqueados); 
	}
	
	synchronized public void WAIT(){
		while (contador == 0){
			try{
				bloqueados.addElement(Thread.currentThread());
				do{
					wait(); 
					/*quiero que salga el que mas tiempo esta esperando*/
					condSalida = bloqueados.firstElement().equals(Thread.currentThread());
					/*Si este thread no puede despertarse, se despierta a otro*/
					if(!condSalida)
						notify();
				}
				while(!condSalida);
				condSalida=false;
				bloqueados.removeElement(Thread.currentThread());
			}
			catch(Exception e){}
		}
		contador--;
	}
	
	public int tamanioBloqueados(){
		return bloqueados.size();
	}
	
	public void imprimirBloqueados(){
		Enumeration<Thread> e=bloqueados.elements();
	    // let us print all the elements available in enumeration
	    while (e.hasMoreElements()) {         
	    	System.out.println(" " + e.nextElement().getName());
	    }           
	}
}
