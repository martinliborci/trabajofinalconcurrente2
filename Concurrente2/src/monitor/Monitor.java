package monitor;
import test.Invariante;
import java.util.Vector;
import main.Politica;
import petri.Petri;
import semaforo.*;

/**
 * @author Pinky
 * 
 */

public class Monitor{
	
	private SemaforoBinarioFIFO mutex;				/* toma el lock del semaforo*/	//para exclusion mutua
	private Vector<SemaforoBinarioFIFO> cc;			//para condiciones de sincronizacion

	/************************PETRI*********************************************/
	public Petri petri;
	public Politica politica; 
	public Invariante invariante;
	/**************************************************************************/
	
	/************************METODOS PUBLICOS**********************************/
	public Monitor() {
		this.mutex	= 	new SemaforoBinarioFIFO(1,100);
		this.petri	=	new Petri();
		this.cc	=	new Vector<SemaforoBinarioFIFO>(petri.columnas);
		initcc(petri.columnas);	
		this.politica=new Politica();
		this.invariante=new Invariante();
	}
	
	public void entrar(int disparo) {
		mutex.WAIT();
		disparar(disparo);
	}
	public void salir() {
		mutex.SIGNAL();
	}
	/************************METODOS PRIVADOS**********************************/
	private void initcc(int transiciones) {
		for (int i = 0;i<transiciones;i++) {
			cc.addElement(new SemaforoBinarioFIFO(0,100));
		}
	}
	
	private void disparar(int disparo){
//		1) averigua si el disparo es posible
//				falso)	se duerme en cc
//			verdadero)	pide trans senciblizadas y dormidos para el AND
//		2) 
//		3) 
		boolean test=false;
		System.out.println("quiero disparar "+ disparo + Thread.currentThread().getName());
		for (int i =0; i< cc.size(); i++) {
			System.out.print(cc.elementAt(i).tamanioBloqueados()+" ");
		}
		System.out.println();
		boolean disparado=petri.nuevoMarcado(disparo); //indica si se pudo disparar o no
		
		// ya se actualizo el nuevo marcado , le testeo los invariante
		
		test=invariante.calcularInvariantePlazas(petri.marcado);
		System.out.println("Resultado de test invariantes de plaza "+test);
		if(disparado) { //pude disparar
			System.out.println("puedo disparar ");
			//calculo dormidos
			int dormidos[]= new int[petri.columnas];
			for (int i=0;i<petri.columnas;i++) {
				dormidos[i]=cc.elementAt(i).tamanioBloqueados();
			}
			//calculo sensibilizados
			int sensibilizadas[]= new int[petri.columnas];
			sensibilizadas=petri.transisionesSensibilizadas();
			//hago el AND
			int m[]=new int[petri.columnas];
			for (int i=0;i<petri.columnas;i++) {
				m[i]=dormidos[i]*sensibilizadas[i];
			}
			//me fijo si m son todos ceros o no
			int n=0;
			for (int i=0;i<m.length;i++) {
				n+=m[i];
			}
			if (n==0) {
				System.out.println("NO puedo despertar de cc ");

				//Dejo libre mutex, despertando de la cola principal si es posible.
				salir();
			}else {
				//mando M a politica
				System.out.println("Puedo despertar de cc ");
				//int t=politica.decidir(m);
				int t = politica.decidir(m,petri.matriz_Prioridad);
				System.out.println("despertar transicion t"+t);
				//despierto de donde me dijo la politica
				cc.elementAt(t).SIGNAL();
				
				return;
			}
			
		}else {	//no pude disparar
			System.out.println("No puedo disparar ");
			salir();
			cc.elementAt(disparo).WAIT();
			disparar(disparo); // vuelvo a llamar al metodo disparar
		}
	}
	
}
