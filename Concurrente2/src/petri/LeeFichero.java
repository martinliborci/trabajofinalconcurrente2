package petri;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;


public class LeeFichero {

		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;
		
		//***********************************
		public int[][] _matriz;
		public int[] _vector;
		private int _filas;
		private int _columnas;
		//***********************************
		
		public LeeFichero(String _archivo) {
			 //crear un ArrayList bidimensional de enteros vac�o
	        //se crea un ArrayList de ArrayLists de enteros
	        ArrayList<ArrayList<Integer>> array = new ArrayList<ArrayList<Integer>>();
	        
	        File file = new File(_archivo);
	        int numero=0, i= 0,j=0,columnas=0,filas=0;	          
	        StringTokenizer st;
	        Scanner entrada = null;
	        String cadena;
	        
	        try {               
	        	entrada = new Scanner(file);
	            while (entrada.hasNext()) {
	            	array.add(new ArrayList<Integer>());
	                cadena = entrada.nextLine();
	                st = new StringTokenizer(cadena, "{ , }");
	            
	                while (st.hasMoreTokens()) {
	                    numero = Integer.parseInt(st.nextToken()); //me toma el entero del archivo
	                    array.get(i).add(numero);
	               }
	               i++;	                            
	            }
	           
	            columnas=(array.get(0)).size();
	            _columnas=columnas;
	            filas=array.size();
	            _filas=filas;
	            
	            int[][] matriz=new int[filas][columnas];
	            for(i=0;i<filas;i++){  //para cada fila
	                for(j=0;j<columnas;j++){  //se recorre todas la columnas de la fila
	                	matriz[i][j]=array.get(i).get(j); //se obtiene el elemento i,j
	                }
	            }
//	            System.out.println("leeFichero "+_archivo);
//	            System.out.println("filas "+filas);
//	            System.out.println("columnas "+columnas);
	            
	            if (filas==1){
	            	_vector=new int[columnas];
	            	for (int v=0;v<columnas;v++){
	            		_vector[v]=matriz[filas-1][v];
	            	}
	            }else{
	            	_matriz=matriz;
	            }
	            
	        } catch (FileNotFoundException e) {
	            System.out.println(e.getMessage());
	            e.printStackTrace();
	        } finally {
	            entrada.close();
	        }
		}	
		int getFilasDeArchivo(){
			return _filas;
		}
		int getColumnasDeArchivo(){
			return _columnas;
		}
}
