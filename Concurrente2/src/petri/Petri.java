package petri;

public class Petri {
	public int filas;     //cantidad de plazas de la red
	public int columnas;  //cantidad de transiciones de la red
	
	public int[][] matriz_Imas;
	public int[][] matriz_Imenos;
	public int[][] matriz_Incidencia;
	public int[][] matriz_Prioridad;
	public int[] marcado;	//Estructura de marcado: 
	
	
	
	public Petri() {
		cargarMatricesDeArchivo();		
	}
	
	/**
	 * METODO TESTEADO, CARGA MATRICES BIEN
	 * @return 
	 */
	public void cargarMatricesDeArchivo(){
		LeeFichero fichero=new LeeFichero("Incidencia");
		filas=fichero.getFilasDeArchivo(); 		//plazas de la red
		columnas=fichero.getColumnasDeArchivo();//cantidad de transiciones de la red
		matriz_Incidencia=fichero._matriz;		//recordar que al cargar las matrices deben ir robots primero y maquinas despues
		
		matriz_Imas= (new LeeFichero("Imas"))._matriz;
		matriz_Imenos=(new LeeFichero("Imenos"))._matriz;
		matriz_Prioridad=(new LeeFichero("Prioridad"))._matriz;

		marcado=(new LeeFichero("MarcadoInicial"))._vector;
		
		System.out.println("filas "+filas);
		System.out.println("columnas "+columnas); 
	}

	//TESTEADO
	public void imprimeMatriz(int[][] matriz){
		System.out.println("---");
		for (int f=0;f<matriz.length;f++){
			for (int c=0; c<matriz[f].length;c++){
				System.out.print(matriz[f][c]);
			}
			System.out.println();
		}
		System.out.println("---");
	}
	public void imprimeMarcado(int[] vector){
		for (int f=0;f<vector.length;f++){
				System.out.print(vector[f]+" ");
		}
		System.out.println("---");
	}
	
	/**METODO TESTEADO, ANDA BIEN
	 * Al vector marcado lo resto por cada columna de la matriz 
	 * Imenos y al resultado (vAux) lo reviso, si encuentro un -1 entonces pongo un 0 en 
	 * vTransicionesSensibilizadas, sino pongo un 1 para indicar que est� sensibilizada
	 * @return vTransicionesSensibilizadas es un vector del tama�o de cantidad de transiciones de 
	 * la red con valores 0 para las transiciones NO sensibilizadas y valores 
	 * 1 para las S� sensibilizadas
	 */
	public int[] transisionesSensibilizadas(){
		int[] vTransisionesSensibilizadas = new int[columnas];
		for (int c=0;c<columnas;c++){
			int[] vAux=new int[filas];
			for (int f=0; f<filas; f++){
				vAux[f]=marcado[f]-matriz_Imenos[f][c];
			}
			vTransisionesSensibilizadas[c]=1;
			for (int i=0;i<filas;i++){
				boolean bandera=false;
				if (vAux[i]<0&&!bandera){
					  vTransisionesSensibilizadas[c]=0;
					  bandera=true;
				}
			}
		}
		return vTransisionesSensibilizadas;
	}
	
/**TESTEADO
 * descripcion: dado un nro de transición, efectúa el disparo y cambia 
 * 				el marcado si está sensibilizada
 * input: 		nro de transición
 * output:		true si se logra cambiar el marcado, false si no estaba sensibilidaza
 */
	public boolean nuevoMarcado(int disparo) {
		boolean bandera=true;
		
		int[] vectorAuxiliar = new int[filas];
		int[] vectorAuxiliar2 = new int[filas];
		int[] vectorTranspuesto = new int[columnas];
		vectorTranspuesto= new int [columnas] ;
		int[] sumador_elementoMatriz = new int[filas];
		for (int s = 0; s < columnas; s++) {
			sumador_elementoMatriz[s] = 0;
			vectorTranspuesto[s] = 0;
		}
		vectorTranspuesto[disparo] = 1;
		for (int i = 0; i < filas; i++) {
			for (int j = 0; j < columnas; j++) {
				sumador_elementoMatriz[i] = sumador_elementoMatriz[i]+ (matriz_Incidencia[i][j] * vectorTranspuesto[j]);
			}
			vectorAuxiliar[i]=sumador_elementoMatriz[i];
			vectorAuxiliar2[i]= marcado[i]+ vectorAuxiliar[i];
		}
		int n=0;
		while(n<filas) {
			if (vectorAuxiliar2[n]==-1) {
				bandera=false;
			}
			n++;
		}
		if (bandera==true) {
			for (int p=0;p<filas;p++) {
				marcado[p]=vectorAuxiliar2[p];
			}
		}
		return bandera;
	}
	
}
